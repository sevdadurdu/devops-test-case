FROM python:3
ADD test-case.py /
RUN pip install flask
EXPOSE 8080
CMD [ "python", "./test-case.py"]
